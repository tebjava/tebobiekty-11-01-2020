package figury;

import interfejsy.Arytmetyka;
import interfejsy.Dzialania;

/* 
 * Przyk�ad definicji klasy nieabstrakcyjnej operuj�cej na dw�ch interfejsach.
 * J�zyk Java, jak to zosta�o wspomniane, pozwala na do��czanie wielu interfejs�w
 * Jedynym problemem b�dzie to, i� musimy przedefiniowa� ("nadpisa�") 
 * wszystkie metody ze wszystkich interfejs�w (zar�wno z Dzialania jak i z 
 * Arytmetyka)
 */

public class Owale implements Dzialania, Arytmetyka{
	/*
	 * Slowo final powoduje, �e "zmienna" staje si�
	 * warto�ci� sta��, kt�rej nie mo�emy zmodyfikowa�
	 * przez ca�y okres dzia�ania programu
	 * 
	 * S�owo static powoduje, �e zmienna/sta�a 
	 * istnieje przez CA�Y OKRES DZIA�ANIA PROGRAMU.
	 * Oznacza to, �e nie musimy tworzy� obiektu z klasy
	 * (poprzez operato new) tylko mo�emy si� do takiej
	 * waarto�ci odwo�a� bezpo�rednio do klasy
	 */
	protected static final double pi=3.14;

	/*
	 * �eby m�c skorzysta� z warto�ci statycznej prywatnej
	 * metoda pobieraj�ca tak� warto�� (b�dz wykorzystujaca ja)
	 * tak�e musi by� statyczna. 
	 */
	public static double getPi() {
		
		return pi;
	}
	
	public Owale() {
		System.out.println("To jest konstruktor Owale!");
	}

	@Override
	public double obwod() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double pole() {
		// TODO Auto-generated method stub
		return 0;
	}
//----------------------------------------------
	//Poni�sze metody i ich implementacja wymuszona jest przez
	//interfejst Arytmetyka
	//W przeciwie�stwie do klas abstrakcyjnych NIE MO�EMY "zdefiniowa�"
	//poni�szych metod jako abstract gdy� nie pracujemy na klasie abstrakcyjnej
	//dlatego te� musieli�my zdefiniowa� minimum (�e funkcje zzwracaj� warto�� 0).
//----------------------------------------------
	@Override
	public double suma(double... ds) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double mnozenie(double... ds) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double dzielenie(double... ds) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double potega(double a, double b) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double pierwiastek(double a, double b) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
