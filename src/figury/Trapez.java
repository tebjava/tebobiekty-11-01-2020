package figury;

import interfejsy.Arytmetyka;

/*
 * Tutaj mamy przyk�ad wykorzystania zar�wo dziedziczenia klasowego jak i
 * implementacji interfejsu
 */
public class Trapez extends Wielokaty implements Arytmetyka{

	public Trapez() {
		super(0,0,0,0,0);
	}
	
	public Trapez(double a, double b, double c, double d, double h) {
		super(a,b,c,d,h);
	}
	
	public Trapez(double...ds) {
		super(ds);
	}
	
	@Override
	public double obwod() {
		// TODO Auto-generated method stub
		return super.obwod(4);
	}
	
	@Override
	public double pole() {
		// TODO Auto-generated method stub
		return (getBoki()[0]+getBoki()[1])*getBoki()[4]/2;
	}



	@Override
	public double mnozenie(double... ds) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double dzielenie(double... ds) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double potega(double a, double b) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double pierwiastek(double a, double b) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double suma(double... ds) {
		// TODO Auto-generated method stub
		return 0;
	}

}
