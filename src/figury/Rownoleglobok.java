package figury;

/*
 * Tutaj mamy przyk�ad dziedziczenia klasy po klasie nieabstracyjnej, kt�ra wcze�niej
 * dziedziczy�a po klasie abstrakcyjnej. W ten oto spos�b mo�na obej�� problem
 * wielodziedziczenia w Java (od wersji 10 mo�na tak�e definiowa� domy�lne zachowania
 * interfejs�w - nie b�dziemy tego rozpatrywa�)
 * Dzi�ki dziedziczeniu nie definiowali�my na nowo metofy obw�d
 */

public class Rownoleglobok extends Prostokat {
	public Rownoleglobok() {
		super(0,0,0);
	}
	
	public Rownoleglobok(double a, double b, double h) {
		super(a,b,h);
	}
	
	@Override
	public double pole() {
		// TODO Auto-generated method stub
		return getBoki()[0]+getBoki()[2];
	}
}
