package figury;

public class Prostokat extends Wielokaty {

	public Prostokat() {
		super(0,0);
	}
	
	public Prostokat(double a, double b) {
		super(a,b);
	}
	
	public Prostokat(double... b) {
		super(b);
	}

	public double obwod() {
		return 2*getBoki()[0] + 2*getBoki()[1];
	}
	
	@Override
	public double pole() {
		
		return getBoki()[0]*getBoki()[1];
	}
	
	public void pokazNazwe() {
		System.out.println("To jest prostokat!");
	}
}
