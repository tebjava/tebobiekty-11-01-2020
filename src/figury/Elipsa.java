package figury;

/*
 * Klasy mog� dziedziczy� po klasach, kt�re z kolei
 * dziecz� po jeszcze innych klasach. Dzi�ki temu
 * tak powsta�a nowa klasa mo�e zawiera� w�a�ciwo�ci
 * wi�cej ni� jednej klasy bazowej (domy�lnie Java
 * pozwala na dziedziczenie jedynie po jednej klasie)
 * INFO: Klasa elipsa mog�a dziedziczy� po klasie
 * Owale.
 */

public class Elipsa extends Okrag {
	private double a,b;
	
	public Elipsa() {
		System.out.println("To jest konstruktor Elipsy!");
	}
	
	/*
	 * Poni�szy argument to inny rodzaj deklaracji zmiennej typu tablicowego
	 * Posiada on jednak jedn� wy�szo�� nad ekwiwalentem, tj.
	 * 
	 * double[] p
	 * 
	 * U�ytkownik (programista) mo�e bez przeszk�d podawa� kolejne warto�ci jako
	 * kolejne parametry metody 
	 * 
	 * Elipsa(13,2.4,1,6.7)
	 * 
	 * Podczas gdy w innym wypadku musieliby�my podwa� tablic� element�w (podstawia� zmienn� tablicow�)
	 * 
	 * Elipsa(new double[] {4,5,6,6,7}) 
	 * 
	 * 
	 */
	public Elipsa(double... p) {
		/*
		 * Poni�ej przyk�ad wywo�ania konstruktora 
		 * klasy bazowej. W ten spos�b mo�emy bazowa� na
		 * obiekcie z odpowiednimi parametrami startowymi
		 * (poni�ej przyk�ad ustawienia warto�ci r klasy
		 * Okrag na warto�� 2)
		 */
		super(2);
		a=p[0];
		b=p[1];
		
	}
	
	public void setA(double a) {
		this.a=a;
	}
	
	public void setB(double b) {
		this.b=b;
	}
	
	
	
	/*
	 * Metoda nadpisuje dzia�ania metody klasy bazowej
	 * Okrag. Je�eli w tym miejscu nie nadpisaliby�my 
	 * tej metody to otrzymywaliby�my pole liczne dla
	 * okr�gu (pola okr�gu dla elipsy nie da si�
	 * w ten spos�b obliczy�). 
	 * W programowaniu powszechne jest nadpisywanie
	 * metod z klas bazowych. 
	 */
	public double pole() {
		System.out.println("Pole klasy Elipsa");
		return pi*a*b;
	}
	
	/*
	 * Metoda nadpisuje zachowanie metody o tej samej 
	 * nazwie z klasy bazowej Okrag (por. komentarz 
	 * powy�ej)
	 */
	public double obwod() {
		System.out.println("Obwod klasy Elipsa");
		return (3/2*(a+b)- Math.pow(a*b,1/2))*pi;
	}
}
