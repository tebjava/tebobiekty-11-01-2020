package figury;

/*
 * Tutaj mamy przyk�ad klasy dziedzicz�cej po innej klasie
 * (tzw. rozszerzenie o w�asno�ci klasy bazowej). W tym wypadku
 * klasa dziedzicz�ca (Okrag) pozyskuje jako swoje 
 * wszystkie sk�adowe publiczne i chronione (protected).
 */
public class Okrag extends Owale {
	private double r;
	
	/*
	 * Poni�sza metoda to tzw. konstruktor klasy. Dzi�ki konstruktorom
	 * programista mo�e wymusi� pewne okre�lone stany i/lub warto�ci 
	 * poszczeg�lnych sk�adowych obecnie konstruowanej klasy. Nawet je�eli
	 * nie zostanie zdefiniowany - KA�DA KLASA POSIADA SWOJ KONSTRUKTOR.
	 * Je�eli nie jest zdefiniowany - tworzony jest tzw. domy�lny (niejawnie - bez wiedzy programisty)
	 * Jakiekolwike utworzenie kt�regokolwiek konstruktora powoduje skasowanie kostruktora
	 * domy�lnego. 
	 * 
	 * Konstruktory mog� przyjmowa� parametry (przyk�ad poni�ej). Mog� posiadac zmienione 
	 * modyfikatory zasi�gu (nie musz� by� public!). Od standardowych metod odr�nia je to, 
	 * �e maj� nazw� identyczn� z nazw� klasy oraz nie posiadaj� zwracanego typu!
	 */
	
	public Okrag() {
		System.out.println("To jest konstruktor Okregu!");
	}
	
	public Okrag(double r) {
		this.r = r;
	}
	
	public void setR(double r) {
		this.r=r;
	}
	
	public double getR() {
		return r;
	}
	
	public double obwod() {
		//poniewa� pi jest pole protected w klasie Owale
		//bez problemu mo�e tutaj zosta� pobrane - nie ma
		//potrzeby pobierania warto�ci poprzez metod�
		System.out.println("Obwod klasy Okrag");
		return 2*pi*r;
	}
	
	public double pole() {
		System.out.println("Pole klasy Okrag");
		return pi*r*r;
	}
}
