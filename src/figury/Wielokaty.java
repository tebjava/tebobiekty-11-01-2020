/*
 * Jest to folder w zr�d�ach, w kt�rym przechowujemy aktualnie edytowany plik z kodem
 * Folder ten znajduje si� w glownym katalogu zrodel naszego projektu
 * Zaleca si� by nasze pliki zawsze znajdowaly sie w paczce; jezeli takowej nie stworzymy
 * (nie utworzymy podfolderu na kod) to wszystko bedzie umieszczane w tzw. default package
 */
package figury;

/*
 * Poniewaz chcemy w naszym kodzie skorzystac z kodu zapisanego w pliku Dzialania.java,
 * kt�ry znajduje si� w innej paczce (folderze) naszego projektu, musimy ten�e plik
 * zaimportowa� (wskaza�, ze chcemy go uzywac); jednak przed jego nazw� musimy poda�
 * nazw� paczki, w kt�rej si� znajduje 
 */
import interfejsy.Dzialania;

/*
 * Poni�ej mamy przyk�ad klasy abstrakcyjnej. Klasa tego typu mo�e by� wykorzystywana
 * do tworzenia innych klas w naszym programie jednak nie mo�emy jej bezpo�rednio wykorzysta�
 * do implementacji zmiennej w naszym programie. Oznacza to, �e klasa mo�e posiada�
 * swoje konstruktory, sk�adowe (zar�wno prywatne, chronione jak i publiczne) oraz kod
 * poszczeg�lnych metod mo�e zosta� zaimplementowany, jednak nie mo�emy wykona� nast�puj�cej
 * akcji:
 * 
 * Czworokaty cz = new Czworokaty(4,5,3);
 * 
 * Ka�da z metod dost�pna w klasie abstrakcyjnej mo�e by� napisana (przeimplementowana)
 * w klasach pochodnych (mo�na zmieni� jej zachowanie i dzia�anie). 
 * 
 * Dla poni�ej klasy u�yto interfejsu (s�owo kluczowe implements pozwala na dodawanie
 * interfejs�w do naszej klasy). Wymusza to na nas zdefiniowanie dzia�ania dw�ch funkcji
 * obw�d oraz pole. 
 * 
 * Poniewa� mamy do czynienia z klas� abstrakcyjn�, z kt�rej b�da korzysta� inne klasy,
 * obw�d mo�emy zaimplementowa� (wskazane poni�ej) gdy� dla dowolnego wielok�ta liczy si�
 * go tak samo (suma wszystkich odcink�w sk�adaj�cych si� na boki).
 * 
 * W przypadku pola ka�de liczone jest indywidualnie. St�d obowi�zek definicji przenosimy
 * na poszczeg�lne klasy (metoda pole - wskazane poni�ej).
 * 
 * Nalezy przy tym pami�tac, �e mo�emy dodawa� dowoln� liczb� interfejs�w do implementacji.
 */
public abstract class Wielokaty implements Dzialania {
	//zmienna prywatna - nawet klasy dziedzicz�ce nie b�d� mia�y do niej bezpo�redniego
	//dost�pu (b�d� mog�y j� modyfikowa� jedynie poprzez odpowiednie metody)
	private double boki[];
	
	//poniewa� mamy do czynienia z klas� abstrakcyjn� mo�emy wywo�anie jej konstruktora
	//uniemo�liwi� publicznie (dodatkowe zabezpieczenie). Dodajemy wi�c modyfikator
	//protected
	//Ustawienie parametru double... powoduje, �e jako parametr mo�emy poda� zar�wno 
	//tablic� jak i zmienne typu double oddzielane przecinkiem (wi�ksza wygoda dla 
	//implementuj�cego kod).
	protected Wielokaty(double... boki) {
		this.boki = boki;
	}
	
	//funkcja zwraca tablic� warto�ci double (wszystkie dodane boki czworok�ta)
	public double[] getBoki() {
		return boki;
	}
	
	//funkcja pozwala na modyfikacj� tablicy zawierajcej boki czworok�ta; 
	//podmienia ona wszystkie dotychczas dodane boki 
	public void setBoki(double... boki) {
		this.boki = boki;
	}
	
	//funkcja nadpisana z interfejsu (modyfikator @Override); dodajemy jej 
	//dzia�anie poprzez definicj� jej cia�a (w interfejsie jest tylko deklaracja 
	//poprzez podanie jej nazwy)
	@Override
	public double obwod() {
		return obwod(boki.length);
	}
	
	//w�a�ciwe obliczanie obwodu; funkcja ta b�dzie u�ywana jedynie w przypadku
	//gdyby poszczeg�lne klasy dziedzicz�ce nie posiada�y implementacji dzia�ania
	//tej�e funkcji
	public double obwod(int l) {
		double ret =0;
		for(int i=0;i<l;i++) {
			ret+=boki[i];
		}
		return ret;
	}
	
	//poniewa� pole czworok�t�w (wielok�t�w) mo�e by� liczone z r�nych wzor�w
	//nie implementujemy tutaj cia�a naszej funkcji a jedynie wskazujemy, �e jes to
	//funkcja abstrakcyjna; wymusza to na programi�cie korzystaj�cym z naszej klasy
	//implementacj� tej�e funkcji (musieli�my j� tutaj doda� poniewa� tego wymaga
	//mechanizm interfejs�w)
	@Override
	public abstract double pole();
}
