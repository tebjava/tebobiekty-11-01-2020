package figury;

//implementacja kwadratu na podstawie klasy Wielok�ty; przeimplementowano
//funkcje obwod, utworzono definicj� pola
public class Kwadrat extends Wielokaty {

	public Kwadrat() {
		//poni�sza funkcja to wywo�anie konstruktora klasy bazowej (w naszym wypadku
		//klasy abstrakcyjnej Wielokaty)
		super(0);
	}
	
	public Kwadrat(double a) {
		super(a);
	}
	
	@Override
	public double obwod() {
		// TODO Auto-generated method stub
		return 4*getBoki()[0];
	}
	
	@Override
	public double pole() {
		return getBoki()[0]*getBoki()[0];
	}

}
