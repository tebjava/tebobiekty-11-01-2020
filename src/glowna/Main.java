/*
 * Odnosnik do materia��w GoogleAndroid:
 * 
 * https://drive.google.com/open?id=11hyY9QPXcDJ3A5k2oUoaASYCxX1JW8e2
 * 
 */
package glowna;

import java.io.IOException;
import java.util.Scanner;

import figury.*;

public class Main {
	public static void main(String[] args) {
	    	while(true) {
	    		//new Main().menu();
	    		menu();
	    	}
	}
	
	@SuppressWarnings("resource")
	static void menu() {
		System.out.println("Program do oblicze� p�l i obwod�w");
		System.out.println("1.Elipsa");
		System.out.println("2.okr�g");
		System.out.println("3.Prostok�t");
		System.out.println("4.Trapez");
		System.out.println("5.Kwadrat");
		System.out.println("6.R�wnoleg�obok");
		System.out.println("7.Zako�czenie programu");
		
		try {
			int wybor = (new Scanner(System.in).nextInt());
			if (wybor>6) 
				System.exit(0);
			/* 
			 * Chocia� nie ma bezpo�redniej mo�liwo�ci utworzenia obiektu
			 * Wielokaty (jest to klasa abstrakcyjna, nie mo�e by� implementowana)
			 * to mo�emy bez problemu tworzy� deklaracj� zmiennej typu Wielokaty.
			 * Nast�pnie w tak utworzon� zmienn� mo�emy "tchn��" �ycie poprzez utworzenie
			 * w niej obiektu, kt�ry na niej bazuje (w napszym przyk�adzie s� to
			 * kolejne wielok�ty utworzone w programie).
			 * 
			 * Nale�y jednak pami�ta�, �e tak utworzony obiekt b�dzie mia� mo�liwo��
			 * korzystania jedynie z metod zadeklarowanych (niekonienie zdefiniowanych)
			 * w klasie abstrakcyjnej. Nie b�dziemy mieli dost�pu do metod unikatowych
			 * dla poszczeg�lnych klas bazuj�cych na Wielokaty. 
			 */
			Wielokaty figura;
			switch(wybor) {
			case 3: figura = new Prostokat(dane(2));
					System.out.print("Obw�d trapezu o bokach ");
					pokazBoki(figura);
					System.out.println(" wynosi: " + figura.obwod());
					System.out.print("Pole trapezu o bokach ");
					pokazBoki(figura);
					System.out.println(" wynosi: " + figura.pole());
					//figura.pokazNazwe();// <- to nie zadzia�a - funkcja istnieje tylko w Prostokat, nie w Wielokaty
					//ponizszy kod by zadzialal - zmienna utworzona jest z klasy
					//-----------------------------------------------------------------
					//Prostokat, posiadajacej odpowiednia definicje metody
					//Prostokat figuraP = new Prostokat(dane(2));
					//figuraP.pokazNazwe();
					break;
			case 4: figura = new Trapez(dane(5));
					System.out.print("Obw�d trapezu o bokach ");
					pokazBoki(figura);
					System.out.println(" wynosi: " + figura.obwod());
					System.out.print("Pole trapezu o bokach ");
					pokazBoki(figura);
					System.out.println(" wynosi: " + figura.pole());
					break;
			case 5: figura = new Kwadrat(dane(1)[0]);
					System.out.println("Obw�d kwadratu o boku " + figura.getBoki()[0] +
							" wynosi: " + figura.obwod());
					System.out.println("Pole kwadratu o boku " + figura.getBoki()[0] +
							" wynosi: " + figura.pole());
					break;
			}
		}
		catch (Exception e) {
			System.err.println("Wprowadzono warto�� inn� ni� liczba ca�kowita (" + e.getMessage() + ")");
			try {
				System.in.read();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}
	
	@SuppressWarnings("resource")
	static double[] dane(int ile) {
		double ret[] = new double[ile];
		for (int i=0;i<ile;i++) {
			if (i==ile-1 && i%2==0 && i!=0) 
				System.out.println("Podaj wysoko�� h: ");
			else
				System.out.println("Podaj bok " + (char)(i+97) + ": ");
			try {
				ret[i]=new Scanner(System.in).nextDouble();
			}
			catch (Exception e) {
				i--;
			}
		}
		
		return ret;
	}
	
	//poni�sza funkcja pozwala na wy�wietlnie poszczeg�lnych bok�w figury 
	//Wielokata bez wiedzy, czym ona jest. Generlanie powinna przypisywa� ka�dej 
	//zmiennej dost�pnej w tablicy boki odpowiedni� liter� alfabetu (zaczynaj�c od a)
	//a gdy mamy nieparzyst� liczb� bok�w to tak�e wysoko�� (h). 
	static void pokazBoki(Wielokaty figura) {
		for (int i=0;i<figura.getBoki().length;i++) {
			if (i==figura.getBoki().length-1 && i%2==0 && i!=0) 
				System.out.print("h: " + figura.getBoki()[i]);
			else {
				//znak wy�wietli si� poniewa� odwo�ujemy si� do znaku
				//zapisanego w kodzie ASCII - do warto�ci i dodajemy 97
				//97 to ma�a litera a w ASCII (kolejne litery alfabetu s� 
				//pouk�adane na kolejnych warto�ciach liczbowych)
				System.out.print((char)(i+97) + ": " + figura.getBoki()[i]);
				if (i!=figura.getBoki().length-1)
					System.out.print( ", ");
			}
		}
	}
}