package interfejsy;

/*
 * Przyk�ad definicji interfejsu w j�zyku Java. Wszystkie funkcje w interfejsie to
 * po prostu deklaracje. Wymuszamy przez to na programistach wykorzystuj�cych nasze
 * interfejsy odpowiedni� definicj� funkcjonalno�ci programu (np. co funkcja
 * ma zwraca�, jakie parametry przyjmowa�) by nast�pnie sam zadecydowa� co dana
 * funkcja ma robi� w jego programie (z podamymi parametrami, jak ma osi�gn�� ostateczny
 * wynik operacji).
 */

public interface Arytmetyka {
	double suma(double...ds);
	double mnozenie(double...ds);
	double dzielenie(double...ds);
	double potega(double a, double b);
	double pierwiastek(double a, double b);
	
}
